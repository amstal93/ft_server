# Dockerfile reference
# https://docs.docker.com/engine/reference/builder/

# Best practices for writing Dockerfiles
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/

# Build and run your image
# https://docs.docker.com/get-started/part2/

# Import Debian image
FROM	debian:buster

# METADATA
LABEL	maintainer="jarnolf@student.21-school.ru" \
		version=4.20

# UPDATE
RUN		apt-get update
RUN		apt-get upgrade -y

# Install extra tools
RUN		apt-get -y install wget
RUN		apt-get -y install zsh
RUN		chsh -s $(which zsh)

# Install NGINX
RUN		apt-get -y install nginx

# Install MySQL
RUN		apt-get -y install mariadb-server

# Install PHP
RUN		apt-get install -y php-fpm php-mysql

# Install OpenSSL
RUN		apt-get install -y openssl

# Copy required files into the container
RUN		mkdir /etc/docker_init
COPY	srcs/start.sh /etc/docker_init/
COPY	srcs/nginx.conf /etc/nginx/sites-available/default
COPY	srcs/wordpress.sql /etc/docker_init/
COPY	srcs/autoindex_toggle.sh /etc/docker_init/
COPY	srcs/.zshrc /root/

# Create a self-signed key and certificate
RUN		openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
		-subj "/C=RU/ST=Moscow/L=Moscow/O=School21/OU=Wave 4/CN=localhost" \
		-keyout /etc/ssl/private/localhost.nginx.key \
		-out /etc/ssl/certs/localhost.nginx.crt
RUN		chmod 600 etc/ssl/certs/localhost.nginx.crt etc/ssl/private/localhost.nginx.key

# Install phpmyadmin
RUN		wget https://files.phpmyadmin.net/phpMyAdmin/5.0.2/phpMyAdmin-5.0.2-all-languages.tar.gz
RUN		tar -xvf phpMyAdmin-5.0.2-all-languages.tar.gz
RUN		rm -rf phpMyAdmin-5.0.2-all-languages.tar.gz
RUN		mv phpMyAdmin-5.0.2-all-languages phpmyadmin
RUN		mv phpmyadmin /var/www/html/
COPY	srcs/config.inc.php /var/www/html/phpmyadmin/config.inc.php

# Configure wordpress
RUN		wget https://wordpress.org/wordpress-5.5.1.tar.gz
RUN		tar -xvf wordpress-5.5.1.tar.gz
RUN		rm -rf wordpress-5.5.1.tar.gz
RUN		mv wordpress/ /var/www/html/
RUN		chmod 775 -R /var/www/html/wordpress
COPY	srcs/wp-config.php /var/www/html/wordpress/

# Setup MySQL
COPY	srcs/mysql_init.sh /etc/docker_init/
COPY	srcs/mysql_secure_installation.sql /etc/docker_init/
COPY	srcs/mysql_wordpress.sql /etc/docker_init/
RUN		sh /etc/docker_init/mysql_init.sh

# Launcher script (being run upon each run of the container)
CMD		sh /etc/docker_init/start.sh

# This instruction doesn't actually publish the port,
# it just provides the information on which port should be published upon running the container.
# i.e. remember to publish ports for http and https by using -p 80:80 -p 443:443
EXPOSE	80 443
