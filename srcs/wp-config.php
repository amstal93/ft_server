<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );
 // define( 'AUTH_KEY',         '[x)9q?7oXXYN`G}s?VT|0O~i>-94sYD3]<2Ui$!,iheK=0|m1ljkhW10C:u/uRQ9' );
// define( 'SECURE_AUTH_KEY',  ':9y7{K%Y|K2tL$U6*_?F}&/p1is5a;&n_d{^n<[;15c[q4Fucao4r+EBd3lPf_=v' );
// define( 'LOGGED_IN_KEY',    '>(*j+r0Pq0qZSND`oEay!9OAQLl(iCjf`ISi{!d&v^*h:-Vi9fU`B1jBMRkn|,<)' );
// define( 'NONCE_KEY',        '8Z=bl-tn7MZo|84m9d@}>u<W=CX[wZ~kPG-:Jv`K3-OW|c0%u-D8R5rR]{P :Grf' );
// define( 'AUTH_SALT',        '0N , C*YT;i^zZED5:u1Sa~gE+XZtp^O^04e^G7zj42NRDWsC(y3<=e:6ti=e&PH' );
// define( 'SECURE_AUTH_SALT', 'uxTQZ=OsQOr60T.z0 PSlr<,Ogf:iJ*rf#gfdR..Po}y)Z}ECNzrg`*zxN(x0$<R' );
// define( 'LOGGED_IN_SALT',   '}J(syb3T<s)_Amp6G2k@h=f>|c7ByphQ6/gmfx}k+Qs!o~;4Lan#2nSWt#&dO+t*' );
// define( 'NONCE_SALT',       ',_f4cD-98$Red?4c0^:@2:T*0@`rzkZU?lv!J${&AnNl$8DpFeLo;|D7PS&-(Jz;' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
