#! bin/bash

# Launch required services
service nginx start
service php7.3-fpm start
service mysql start
#launch the shell so the container doesn't stop after running previous commands.
bin/zsh
