CREATE DATABASE wordpress;

GRANT ALL PRIVILEGES ON wordpress.* TO 'root'@'localhost' WITH GRANT OPTION;
-- Use the WITH GRANT OPTION clause to give users the ability to grant privileges to other users at the given privilege level.
-- Users with the GRANT OPTION privilege can only grant privileges they have.

UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE user='root';

-- Reloading the privilege tables will ensure that all changes made so far will take effect immediately.
FLUSH PRIVILEGES;
